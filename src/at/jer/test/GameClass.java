package at.jer.test;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.RoundedRectangle;

import java.awt.*;

public class GameClass extends BasicGame {

    private Shape rect;

    public GameClass(String title) {
        super(title);
        this.rect = new Rectangle(100, 100, 100, 100);

    }



    @Override
    public void init(GameContainer gameContainer) throws SlickException {

    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        if(i > 8) {
            this.rect.setX(this.rect.getX() + 10);
            this.rect.setY(this.rect.getY() + 10);
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.drawString("Test", 1, 2);
        graphics.draw(rect);
    }
}
