package at.jer.wrdGäs;


import org.lwjgl.Sys;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.net.*;
import java.util.concurrent.TimeUnit;

public class Game extends BasicGame {
    private String word;
    private ArrayList<String> correctGuesses;
    private ArrayList<String> falseGuesses;
    private String title;
    private LifesCounter lifesCounter;
    private String encryptedWord;
    private Boolean finished;
    private Boolean restart;

    public Game(String title) {
        super(title);

    }


    public String encrytWord() {
        String[] splittedWord =this.word.split("");
        ArrayList<Integer> indexes= new ArrayList<>();
        int index = 0;
        for (String character:splittedWord) {

            for(String chara: correctGuesses) {
                if(chara.equals(character)) {
                    indexes.add(index);
                }
            }
            splittedWord[index] = "*";
            index++;

        }
        for(Integer i:indexes) {
            String[] splittedWord2 =this.word.split("");
            splittedWord[i] = splittedWord2[i];
        }
        if(indexes.size() == this.word.split("").length) {
            finished = true;
        }
        String s = String.join("", splittedWord);
        return s;
    }


    public static String getWord() throws Exception {
        String urlToRead = "https://random-word-api.herokuapp.com/word?number=1&swear=0";
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString().replaceAll("\\p{P}", "");
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.finished = false;
        this.restart = false;
        this.correctGuesses = new ArrayList<>();
        this.falseGuesses = new ArrayList<>();
        try {
            this.word = getWord();
            this.encryptedWord = encrytWord();
            System.out.println(this.word);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.lifesCounter = new LifesCounter();
    }


    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        lifesCounter.update(i);
        this.encryptedWord = encrytWord();

        if(restart) {
            gameContainer.reinit();
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        lifesCounter.render(graphics);
        lifesCounter.setLifesLeft(8 - this.falseGuesses.size());
        graphics.drawString(this.encryptedWord, 300, 100);
        graphics.drawString("False Counter:"+ this.falseGuesses.size(), 100, 100);
        if(this.finished) {
            graphics.drawString("Gewonnen!! Neu starten mit Backspace", 300, 400);
        }
        if(lifesCounter.getLifesLeft() == 0) {
            graphics.drawString("Verloren!! Neu starten mit Backspace", 300, 400);
        }

    }

    public void keyPressed(int key, char c) {
        if(key == 14) {
            restart = true;
            return;
        }
       // System.out.println(key);
        String[] splittedString = this.word.split("");
        boolean rightChar = false;
        for(String cha: splittedString) {
           // System.out.println(cha+ " : "+ String.valueOf(c));
            if(cha.equals(String.valueOf(c))){

                rightChar = true;
            }
        }
        if(rightChar) {
            System.out.println("richtiger Char gefunden");
            boolean alreadyInArray = false;
            for(String cha: correctGuesses) {
                if(cha.equals(String.valueOf(c))) {
                    alreadyInArray = true;
                }
            }
            if(!alreadyInArray){

                correctGuesses.add(String.valueOf(c));
                System.out.println(correctGuesses);
            }

        }
        else {
            boolean alreadyInArray = false;
            for(String cha: falseGuesses) {
                if(cha.equals(String.valueOf(c))) {
                    alreadyInArray = true;
                }
            }
            if(!alreadyInArray){
                falseGuesses.add(String.valueOf(c));
            }
        }

    }
}
